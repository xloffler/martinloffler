<html>
    <head>
        <title>Bruh</title>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
            function tabulka($sloupce, $radky) {
                echo "<table border=\"1\">";

                for ($i = 1; $i <= $radky; $i++) {
                    echo "<tr>";
                    for ($j = 1; $j <= $sloupce; $j++) {
                        echo "<td>" . $i . "-" . $j . "</td>";
                    }
                    echo "</tr>";
                }

                echo "</table>";
            }

            tabulka(3, 3);

            /**
             * Vrátí věk podle rodného čísla
             * @author Martin Löffler (FatalErrorCoded)
             * @param string $pin Rodné číslo
             * @return int
             */
            function getAge($pin) {
                $year = substr($pin, 0, 2);
                $month = substr($pin, 2, 2);
                $day = substr($pin, 4, 2);

                $decrement = 0;
                echo "<br>" . $day . ", " . date("d") . " ? " . $day > date("d") . "<br>";
                if ($month > date("m") && $day > date("d")) $decrement++;

                if ($year < date("y")) { return date("y") - $year - $decrement; }
                else if ($year > date("y")) { return 0; }
            }

            var_dump(getAge("890101/1111"));
            var_dump(getAge("030310/2222"));
        ?>
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <title>Funkce PHP</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h3>Matematické funkce</h3>
        <?php
            echo abs($a);
            echo "<br><br>";

            $numbers = [1, 2, -1, 4, 6];
            echo min($numbers);
            echo "<br>" . max($numbers);
            echo "<br><br>";

            echo round(1.4);
            echo "<br><br>";

            echo rand();
            echo "<br><br>";
        ?>
        <h3>Práce s řetězci</h3>
        <?php
            //echo '<b>explode</b>(", ", "ahoj, jak se máš, mám se dobře")<br>';
            var_dump(explode(", ", "ahoj, jak se máš, mám se dobře"));
            echo '<br><br>';

            //echo '<b>implode</b>(", ", ["1", "2", "3"])<br>';
            var_dump(implode(", ", ["1", "2", "3"]));
            echo '<br><br>';

            $lorem = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.";

            echo strchr($lorem, "sit"); // strchr je alias strstr
            echo "<br>";
            echo strlen($lorem);
            echo "<br>";
            echo strpos($lorem, "sit");
            echo "<br>";

            echo md5($lorem);
            echo "<br><br>";

            echo strtolower($lorem);
            echo "<br>" . strtoupper($lorem);
            echo "<br><br>";

            echo substr($lorem, 10, 30);
            echo "<br><br>";

            $text = "tohle bylo napsáno malými písmeny";
            echo ucfirst($text);
            echo "<br>" . ucwords($text);
            echo "<br><br>";
        ?>
        <h3>Práce s poli</h3>
        <?php
            var_dump(array_merge([2, 4, 6], [1, 3, 5]));
            echo "<br><br>";

            var_dump(array_values([2, 4, 6]));
            echo "<br><br>";

            echo "<table border=\"1\">";
            foreach ($numbers as $key => $number) {
                echo "<tr>";
                echo "<td>" . $key . "</td>";
                echo "<td>" . $number . "</td>";
                echo "</tr>";
            }
            echo "</table><br>";

            echo sizeof($numbers);
            echo "<br><br>";

            sort($numbers);
            echo "<table border=\"1\">";
            foreach ($numbers as $key => $number) {
                echo "<tr>";
                echo "<td>" . $key . "</td>";
                echo "<td>" . $number . "</td>";
                echo "</tr>";
            }
            echo "</table>";
        ?>
        <h3>Vlastní funkce isValid</h3>
        <?php
            function isValid($text, $vulgarArr): bool {
                foreach ($vulgarArr as $word) {
                    if (strpos(" " . $text . " ", " " . $word . " ") !== false) {
                        return false;
                    }
                }
                return true;
            }
            var_dump(isValid("ahoj ahoj ahoj ahoj", ["kredent"]));
            var_dump(isValid("ahoj ahoj ahoj dement kredent ahoj", ["kredent"]));

            if (!isset($_POST["text"]) || !isset($_POST["vulgar"])) {
                ?>
                    <br>
                    <form method="POST">
                        <label>Text:</label><br>
                        <textarea name="text"></textarea><br>
                        <label>Sprostá slova:</label><br>
                        <input type="text" name="vulgar"><br>
                        <input type="submit">
                    </form>
                <?php
            } else {
                echo "<br><br>";
                var_dump(isValid($_POST["text"], explode(", ", $_POST["vulgar"])));
            }
        ?>
    </body>
</html>